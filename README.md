## About Gamer's Wiki

This is a web application built in **[Laravel](https://laravel.com)** and **[Tailwind Css](https://tailwindcss.com/)** framework with the **[IGDB](https://www.igdb.com/discover)** API showing:

- Popular Games
- Recent Games
- Coming Soon Games
- Most Anticipated

This build was off **[Andre's video series](https://laracasts.com/series/build-a-video-game-aggregator)**

## ScreenShots

![Screenshot](public/screen-shot1.png)

![Screenshot](public/screen-shot2.png)

### Installation

- Clone the repo and cd into it
- composer install
- npm install
- npm run dev
- Rename or copy .env.example file to .env
- Set your IGDB_KEY in your .env file. You can get an **[API](https://api-docs.igdb.com/#about)** key here.
- php artisan key:generate
- php artisan serve 
- Visit localhost:8000 in your browser

### Contact
Feel free to reach out to me on **[Mail](mailto:kip.kevin.kk@gmail.com?subject=[BitBucket]-Gamer's wiki)** for any other questions or comments on this project.

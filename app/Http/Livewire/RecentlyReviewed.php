<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Livewire\Component;

class RecentlyReviewed extends Component
{
    public $recentlyReviewed = [];

    public function loadRecentlyReviewed(){
        $before = Carbon::now()->subMonths(2)->timestamp;
        $current = Carbon::now()->timestamp;

       $recentlyReviewedUnformatted = Http::withHeaders(config('services.igdb'))->withBody(
            "
            fields name, cover.url, first_release_date,
            total_rating_count,platforms.abbreviation,slug,
            rating,rating_count,summary;
            where platforms = (48,49,130,6)
            &(first_release_date >= {$before}
            &first_release_date < {$current}
            &rating_count < 5);
            sort total_rating_count desc;
            limit 4;
            ",
            'text/plain'
        )->post('https://api.igdb.com/v4/games/')
            ->json();

        $this->recentlyReviewed = $this->formatToView($recentlyReviewedUnformatted);
    }

    public function render()
    {
        return view('livewire.recently-reviewed');
    }

    private function formatToView($games): array
    {
        return collect($games)->map(function ($game){
            return collect($game)->merge([
                'coverImageUrl' => isset($game['cover']) ? Str::replaceFirst('thumb','cover_big', $game['cover']['url']) : 'poster.jpg',
                'rating' => isset($game['rating']) ? round($game['rating']) . '%' : 0 . '%',
                'platform' => collect($game['platforms'])->pluck('abbreviation')->implode(', ')
            ]);
        })->toArray();
    }
}

<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Livewire\Component;

class PopularGames extends Component
{
    public $popularGames = [];

    public function loadPopularGames(){

        $before = Carbon::now()->subMonths(2)->timestamp;
        $after = Carbon::now()->addMonths(2)->timestamp;

        $popularGamesUnformatted = Cache::remember('popular-games', 10, function () use ($after, $before) {
            return Http::withHeaders(config('services.igdb'))->withBody(
                "
            fields name, cover.url,rating,first_release_date,
            slug,total_rating_count,platforms.abbreviation;
            where platforms = (48,49,130,6)
            &(first_release_date >= {$before}
            &first_release_date < {$after}
            );
            sort total_rating_count desc;
            limit 12;
            ",
                'text/plain' )
                ->post('https://api.igdb.com/v4/games/')
                ->json();
        });

//        dd($this->formatForView($popularGamesUnformatted));

        $this->popularGames = $this->formatForView($popularGamesUnformatted);

    }

    public function render()
    {
        return view('livewire.popular-games');
    }

    private function formatForView ($games): array
    {
        return collect($games)->map(function ($game){
            return collect($game)->merge([
                'coverImageUrl' => isset($game['cover']) ? Str::replaceFirst('thumb','cover_big', $game['cover']['url']) : 'poster.jpg',
                'rating' => isset($game['rating']) ? round($game['rating']). '%' : null,
                'platforms' => collect($game['platforms'])->pluck('abbreviation')->implode(', ')
            ]);
        })->toArray();
    }
}

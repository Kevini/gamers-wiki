<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;


class GamesController extends Controller
{

    public function index()
    {
        return view('index');
    }

    public function show($slug)
    {
        $game = Http::withHeaders(config('services.igdb'))->withBody(
        "
            fields *, cover.url, genres.*,involved_companies.company.*,
            platforms.abbreviation,rating,screenshots.*,similar_games.*,
            videos.*,summary;
            where slug = \"{$slug}\" ;
            ", 'text/plain'
        )->post('https://api.igdb.com/v4/games/')->json();

        abort_if(!$game, 404);

       return view('show',[
           'game' => $this->formatForView($game[0]),
       ]);
    }

    private function formatForView($game): array{
      return collect($game)->merge([
            'coverImageUrl' => isset($game['cover']) ? Str::replaceFirst('thumb','cover_big', $game['cover']['url']) : 'poster.jpg',
            'genre'=> collect($game['genres'])->pluck('name')->implode(', '),
            'involvedCompanies' => $game['involved_companies'][0]['company']['name'] ?? null,
            'platform' => collect($game['platforms'])->pluck('abbreviation')->implode(', '),
            'memberRating' => isset($game['rating']) ? round($game['rating']) : 0,
            'criticRating'=> array_key_exists('aggregated_rating',$game) ? round($game['aggregated_rating']) : 0,
            'trailer' => array_key_exists('videos', $game) ? 'https://www.youtube.com/watch?v='. $game['videos'][0]['video_id'] : null,
            'screenshots'=> array_key_exists('screenshots',$game) ? collect($game['screenshots'])->map(function ($screenshot){
                return[
                    'big' => Str::replaceFirst('thumb','screenshot_big', $screenshot['url'] ),
                    'huge'=> Str::replaceFirst('thumb','screenshot_huge',$screenshot['url'] ),
                ];
            })->take(6) : '',
        ])->toArray();
    }

}

<div wire:init="loadPopularGames" class="popular-games text-sm grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 xl:grid-cols-6 gap-12 pb-16 border-b border-gray-800">
    @forelse($popularGames as $game)
        <x-game-card :game="$game"/>
        @empty
        @foreach(range(1,12) as $game)
        <div class="game mt-8">
            <div class="relative inline-block animate-pulse">
              <div class="bg-gray-800 w-40 h-56"></div>
            </div>
            <a href="#" class="block text-transparent text-lg bg-gray-700 font-semibold leading-tight mt-4 animate-pulse">Title Goes here</a>
            <div class="text-transparent bg-gray-700 rounded inline-block mt-3 animate-pulse">PS4, PC & Switch</div>
        </div>
        @endforeach
    @endforelse
</div>

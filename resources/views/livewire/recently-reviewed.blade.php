<div wire:init="loadRecentlyReviewed" class="recently-reviewed-container space-y-12 mt-8">
                @forelse($recentlyReviewed as $recent)
                <div class="game bg-gray-800 rounded-lg flex px-6 py-6">
                    <div class="relative flex-none">
                        <a href="{{route('games.show', $recent['slug'])}}">
                            <img src="{{$recent['coverImageUrl']}}" alt="game cover"
                                 class=" w-48 hover:opacity-75 transition-ease-in-out duration-150">
                        </a>

                            <div class="absolute bottom-0 right-0 w-16 h-16 bg-gray-900 rounded-full"
                                 style="right: -20px; bottom: -20px">
                                <div class="text-semibold text-sm flex justify-center items-center h-full">
                                    {{$recent['rating']}}
                                </div>
                            </div>

                    </div>
                    <div class="ml-6 lg:ml-12">
                        <a href="{{route('games.show', $recent['slug'])}}" class="block text-lg font-semibold leading-tight hover:text-gray-500 mt-4">{{$recent['name']}}</a>
                        <div class="text-gray-400 mt-1">
                            {{$recent['platform']}}
                        </div>
                        <p class="mt-6 text-gray-400 hidden lg:block">
                            {{$recent['summary']}}
                        </p>
                    </div>
            </div>
        @empty
        @foreach(range(1,4) as $recent)
                <div class="game bg-gray-800 rounded-lg flex px-6 py-6">
                    <div class="relative flex-none">
                        <div class="bg-gray-700 w-32 lg:w-48 h-40 lg:h-56 animate-pulse"></div>
                    </div>
                    <div class="ml:6 lg:ml-12">
                        <div class="inline-block text-lg font-semibold leading-tight text-transparent bg-gray-700 mt-4"> Title goes here </div>

                        <div class="mt-6 text-gray-400 space-y-4 hidden lg:block animate-pulse">
                            <span class="text-transparent bg-gray-700">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet autem dolore odio praesentium quam.
                            </span>
                            <span class="text-transparent bg-gray-700">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet autem dolore odio praesentium quam.
                            </span>
                            <span class="text-transparent bg-gray-700">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet autem dolore odio praesentium quam.
                            </span>
                         </div>
                    </div>
            </div>
        @endforeach
        @endforelse
    </div>

<div class="relative" x-data="{ isVisible : true } " @click.away="isVisible = false">
    <input
        wire:model.debounce.300ms="search"
        type="text" class="bg-gray-800 text-sm rounded-full px-4 pl-8 py-1 w-60
                focus:outline-none focus:shadow-outline" placeholder="search"
        @focus= "isVisible = true"
        @keydown.escape.window= "isVisible = false"
        @keydown= "isVisible = true"
    >
    <div class="absolute top-2 h-full ml-2">
        <svg class="text-gray-400 w-4" fill="none" stroke="currentColor" viewBox="0 0 24 24"
             xmlns="http://www.w3.org/2000/svg">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                  d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
        </svg>
    </div>
    <div wire:loading class="spinner top-0 right-0 mr-4 mt-3" style="position: absolute"></div>
    @if(strlen($search >= 2))
        <div class="absolute z-50 bg-gray-800 text-xs rounded w-60 mt-2" x-show.transition.opacity.duration.300="isVisible">
            @if(count($searchResults) > 0)
                <ul>
                    @foreach($searchResults as $result)
                        <li class="border-gray-700 border-b">
                            <a href="{{route('games.show', $result['slug']) }}"
                               class="block flex items-center transition ease-in-out duration-150 hover:bg-gray-700 px-3 py-3">
                                @if(isset($result['cover']))
                                    <img
                                        src="{{ Illuminate\Support\Str::replaceFirst('thumb','cover_small', $result['cover']['url']) }}"
                                        alt="cover" class="w-10">
                                @else
                                    <img src="https://via.placeholder.com/264x352" alt="game cover" class="w-10">
                                @endif
                                <span class="ml-4">{{$result['name']}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            @else
                <div class="py-3 px-3"> No search results for {{ $search }}</div>
            @endif
        </div>
    @endif
</div>

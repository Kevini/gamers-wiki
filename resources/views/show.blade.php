@extends('layouts.app')
@section('content')
<div class="container mx-auto px-4">
    <div class="game-details border-b border-gray-800 pb-12 flex flex-col lg:flex-row">
      <div class="flex-none">
          <img src="{{ $game['coverImageUrl']}}" alt="cover">
      </div>
        <div class="lg:ml-12 lg:mr-64">
            <h2 class="font-semibold text-4xl leading-tight mt-1 ">{{ $game ['name']}}</h2>
            <div class="text-gray-400">
                <span>
                    {{$game['genre']}}
                </span>
                &middot;
                <span>{{$game['involvedCompanies']}}</span>
                &middot;
                <span>
                    {{$game['platform']}}
                </span>
            </div>
            <div  class="flex flex-wrap items-center mt-8">
                <div class="flex items-center">
                    <div id="memberRating" class="w-16 h-16 rounded-full bg-gray-800 relative text-sm">
                        @push('scripts')
                        @include('_rating', [
                            'slug' => 'memberRating',
                            'rating'=> $game['memberRating'],
                            'event'=> null,
                        ])
                        @endpush
{{--                        <div class="font-semibold text-xs flex justify-center items-center h-full">--}}
{{--                           {{$game['memberRating']}}--}}
{{--                        </div>--}}
                    </div>
                    <div class="ml-4 text-xs">
                        Member <br> Score
                    </div>

                    <div class="flex items-center ml-12">
                        <div id="criticRating" class="w-16 h-16 rounded-full bg-gray-800 relative text-sm">
                            @push('scripts')
                                @include('_rating', [
                                    'slug' => 'criticRating',
                                    'rating'=> $game['criticRating'],
                                    'event'=> null,
                                ])
                            @endpush

{{--                            <div class="font-semibold text-xs flex justify-center items-center h-full">--}}
{{--                                {{$game['criticRating']}}--}}
{{--                            </div>--}}
                        </div>
                        <div class="ml-4 text-xs">
                            Critic <br> Score
                        </div>
                    </div>

                    <div class="flex items-center space-x-4 lg:ml-12">
                        <div class="w-8 h-8 bg-gray-800 rounded-full flex items-center justify-center">
                            <a href="#" class="hover:text-gray-400">
                                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/></svg>
                            </a>
                        </div>
                        <div class="w-8 h-8 bg-gray-800 rounded-full flex items-center justify-center">
                            <a href="#" class="hover:text-gray-400">
                                <svg class="w-5 h-5 fill-current" viewBox="0 0 16 18" fill="none"><g clip-path="url(#clip0)"><path d="M8.004 4.957c-2.272 0-4.104 1.804-4.104 4.04 0 2.235 1.832 4.039 4.104 4.039 2.271 0 4.103-1.804 4.103-4.04 0-2.235-1.832-4.039-4.103-4.039zm0 6.666c-1.468 0-2.668-1.178-2.668-2.627 0-1.448 1.196-2.626 2.668-2.626 1.471 0 2.667 1.178 2.667 2.626 0 1.449-1.2 2.627-2.667 2.627zm5.228-6.831a.948.948 0 01-.957.942.948.948 0 01-.957-.942.95.95 0 01.957-.942.95.95 0 01.957.942zm2.718.956c-.06-1.262-.354-2.38-1.293-3.301-.936-.921-2.071-1.21-3.353-1.273C9.982 1.1 6.02 1.1 4.7 1.174c-1.279.06-2.414.348-3.354 1.27-.939.92-1.228 2.038-1.292 3.3-.075 1.301-.075 5.2 0 6.5.06 1.263.353 2.381 1.292 3.302.94.921 2.072 1.21 3.354 1.273 1.321.074 5.282.074 6.604 0 1.282-.06 2.417-.348 3.353-1.273.936-.921 1.229-2.039 1.293-3.301.075-1.3.075-5.196 0-6.497zm-1.707 7.893a2.68 2.68 0 01-1.522 1.497c-1.053.412-3.553.317-4.717.317-1.165 0-3.668.091-4.718-.317a2.68 2.68 0 01-1.522-1.497c-.418-1.037-.321-3.498-.321-4.645 0-1.146-.093-3.61.321-4.644a2.68 2.68 0 011.522-1.497c1.053-.412 3.553-.317 4.718-.317 1.164 0 3.667-.091 4.717.317.7.274 1.24.805 1.522 1.497.418 1.037.321 3.498.321 4.644 0 1.147.097 3.611-.321 4.645z" /></g><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h16v18H0z"/></clipPath></defs></svg>
                            </a>
                        </div>
                        <div class="w-8 h-8 bg-gray-800 rounded-full flex items-center justify-center">
                            <a href="#" class="hover:text-gray-400">
                                <svg class="w-5 h-5 fill-current" viewBox="0 0 18 18" fill="none"><path d="M6.382 15h-.06a8.152 8.152 0 01-3.487-.818 1.035 1.035 0 01-.585-1.08 1.057 1.057 0 01.87-.885 4.972 4.972 0 001.905-.667 7.117 7.117 0 01-2.633-6.803 1.058 1.058 0 01.75-.862 1.012 1.012 0 011.073.308 5.317 5.317 0 003.66 2.062A3.375 3.375 0 018.932 3.93a3.352 3.352 0 014.778.142.525.525 0 00.585.075 1.043 1.043 0 011.455 1.2 4.993 4.993 0 01-.96 1.95A8.093 8.093 0 016.382 15zm0-1.5h.06a6.592 6.592 0 006.818-6.442.99.99 0 01.277-.638c.183-.232.34-.483.465-.75a1.92 1.92 0 01-1.432-.638 1.836 1.836 0 00-1.32-.532 1.875 1.875 0 00-1.343.518 1.897 1.897 0 00-.54 1.814l.195.856-.877.06a6.225 6.225 0 01-4.905-1.8 5.34 5.34 0 002.797 4.845l.713.405-.473.675a4.216 4.216 0 01-2.01 1.44 6.25 6.25 0 001.568.187h.007z" /></svg>
                            </a>
                        </div>
                        <div class="w-8 h-8 bg-gray-800 rounded-full flex items-center justify-center">
                            <a href="#" class="hover:text-gray-400">
                                <svg class="w-5 h-5 fill-current" viewBox="0 0 14 16" fill="none"><path d="M14 2.5v11a1.5 1.5 0 01-1.5 1.5H9.834V9.463h1.894L12 7.35H9.834V6c0-.612.17-1.028 1.047-1.028H12V3.084A15.044 15.044 0 0010.369 3C8.756 3 7.65 3.984 7.65 5.794v1.56h-1.9v2.112h1.903V15H1.5A1.5 1.5 0 010 13.5v-11A1.5 1.5 0 011.5 1h11A1.5 1.5 0 0114 2.5z" /></svg>
                            </a>
                        </div>
                    </div>

                </div>
                @if(array_key_exists('summary', $game))
                <p class="mt-12">
                    {{$game['summary']}}
                </p>
                @endif
                <div class="mt-12">
                    <a href="{{$game['trailer']}}" class = "flex bg-blue-500 text-white font-semibold px-4 py-4 hover:bg-blue-600 rounded transition-ease-in-out duration-150">
                        <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14.752 11.168l-3.197-2.132A1 1 0 0010 9.87v4.263a1 1 0 001.555.832l3.197-2.132a1 1 0 000-1.664z"/><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/></svg>
                        <span class="ml-2">Play Trailer</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Game Details-->

    <div class="image-container border-b border-gray-800 pb-12 mt-8">
        <h2 class="text-blue-500 uppercase tracking-wide font-semibold">Images</h2>
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12 mt-8">
            @foreach($game['screenshots'] as $screenshot)
            <div>
                <a href="{{ $screenshot['huge'] }}">
                    <img src="{{$screenshot['big']}}" alt="screenshot" class="hover:opacity-75 transition ease-in-out duration-150">
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <!-- End images Container -->
    <div class="similar-games-container pb-12 mt-8">
        <h2 class="text-blue-500 uppercase tracking-wide font-semibold">Similar Games</h2>
        <div class="popular-games text-sm grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 xl:grid-cols-6 gap-12">
            @foreach ($game['similar_games'] as $similar)
            <div class="game mt-8">
                <div class="relative inline-block">
                    @if(array_key_exists('cover', $similar))
                    <a href="#">
                        <img src="poster.jpg" alt="game cover" class="hover:opacity-75 transition-ease-in-out duration-150">
                    </a>
                    @endif
                    <div class="absolute bottom-0 right-0 w-16 h-16 bg-gray-800 rounded-full"
                         style="right: -20px; bottom: -20px">
                        @if(array_key_exists('rating', $similar))
                        <div class="text-semibold text-sm flex justify-center items-center h-full">
                            {{ round($similar['rating'])}}
                        </div>
                        @endif
                    </div>
                </div>
                <a href="" class="block text-base font-semibold leading-tight hover:text-gray-500 mt-8">{{$similar['name']}}</a>
                <div class="text-gray-400 mt-1">
                    platform
                </div>
            </div>

            @endforeach
        </div>
    </div>

</div>
@endsection

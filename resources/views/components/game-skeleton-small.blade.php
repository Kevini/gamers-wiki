<div class="game flex">
    <div class="inline-block animate-pulse">
        <div class="bg-gray-800 w-20 h-28"></div>
    </div>
    <div class="ml-4">
        <p class="bg-gray-800 mt-1 text-lg text-transparent animate-pulse">Movie title</p>
        <p class="bg-gray-800 inline-block mt-2 rounded text-lg text-transparent animate-pulse">Date her</p>
    </div>
</div>
